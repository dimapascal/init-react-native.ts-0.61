/**
 * @format
 */

import App from './App';
import {AppRegistry} from 'react-native';
import Reactotron from 'reactotron-react-native';
import {name as appName} from './app.json';

if (__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
  console.tron = Reactotron;
}

AppRegistry.registerComponent(appName, () => App);
