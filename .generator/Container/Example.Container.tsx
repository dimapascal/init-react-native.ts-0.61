import React, {Component, Dispatch} from 'react';
import {SafeAreaView, Text, View} from 'react-native';

import {AnyAction} from 'redux';
import ExampleStyles from './Example.Styles';
import IDefaultProps from 'src/Interface/IDefaultProps';
import {IRootReducer} from '../../Interface/IRootReducer';
import {InferProps} from 'prop-types';
import {connect} from 'react-redux';

type Props = {};

const style = ExampleStyles;

class ExampleContainer extends Component<InferProps<Props> & IDefaultProps> {
  render() {
    return (
      <SafeAreaView style={style.content}>
        <View>
          <Text>Name container ready</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: IRootReducer) => ({});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ExampleContainer);
