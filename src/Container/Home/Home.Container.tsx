import React, {Component, Dispatch} from 'react';
import {SafeAreaView, Text, View} from 'react-native';

import {AnyAction} from 'redux';
import HomeStyles from './Home.Styles';
import IDefaultProps from 'src/Interface/IDefaultProps';
import {IRootReducer} from '../../Interface/IRootReducer';
import {InferProps} from 'prop-types';
import WelcomeComponent from '../../Component/Welcome/Welcome.Component';
import {connect} from 'react-redux';

type Props = {};

const style = HomeStyles;

class HomeContainer extends Component<InferProps<Props> & IDefaultProps> {
  render() {
    return (
      <SafeAreaView style={style.content}>
        <View>
          <Text>Name container ready</Text>
          <WelcomeComponent />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: IRootReducer) => ({});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
