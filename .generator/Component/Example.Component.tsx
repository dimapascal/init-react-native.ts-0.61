import PropTypes, {InferProps} from 'prop-types';
import {Text, View} from 'react-native';

import ExampleStyles from './Example.Styles';
import React from 'react';

const propTypes = {
  myProp: PropTypes.any,
};

const defaultProps = {
  myProp: null,
};

const style = ExampleStyles;

/**
 * @param {object} props
 * @param {any} props.myProp
 */
function ExampleComponent(props: InferProps<typeof propTypes>): JSX.Element {
  const {myProp} = props;

  return (
    <View style={style.content}>
      <Text>Example component is ready!</Text>
    </View>
  );
}

ExampleComponent.propTypes = propTypes;
ExampleComponent.defaultProps = defaultProps;

export default ExampleComponent;
