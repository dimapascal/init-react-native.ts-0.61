import {Action, AnyAction, Reducer} from 'redux';

export interface IRootReducer {
  authReducer: Reducer<Record<string, any>, Action<AnyAction>>;
}
