import {applyMiddleware, compose, createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';

import AsyncStorage from '@react-native-community/async-storage';
import {authReducer} from './Auth.Reducer';
import {combineReducers} from 'redux';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  authReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: [],
  blacklist: [],
};

let middleware = [thunk];

if (process.env.NODE_ENV === `development`) {
  const {logger} = require(`redux-logger`);
  middleware.push(logger);
}

export default function configureStore() {
  const enhancer = compose(applyMiddleware(...middleware));
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, enhancer);
  const persistor = persistStore(store);
  return {store, persistor};
}
