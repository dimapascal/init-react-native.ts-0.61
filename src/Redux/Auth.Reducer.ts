import {AnyAction} from 'redux';

const initialState: Record<string, any> = {};

export enum AuthActionTypes {
  SET_AUTH = 'SET_AUTH_REDUCER_ACTION',
}

export function setAuth(value: any): AnyAction {
  return {type: AuthActionTypes.SET_AUTH, payload: value};
}

export const authReducer = (state = initialState, action: AnyAction) => {
  if (AuthActionTypes.SET_AUTH === action.type) {
    return {...state, ...action.payload};
  }
  return state;
};
